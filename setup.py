from setuptools import setup

setup(
    name="wizzard",
    version="0.0.1",
    description="Media files packet converter",
    url="https://bitbucket.org/shmyga/wizzard",
    author="shmyga",
    author_email="shmyga.z@gmail.com",
    license="MIT",
    package_dir={"": "src"},
    packages=["wizzard"],
    scripts=["main.py"],
    install_requires=[
        "argparse>=1.4.0"
        "mutagen>=1.44.0"
        "Pillow>=7.1.2"
        "tqdm>=4.46.0"
    ],
    entry_points={
        "console_scripts": [
            "wizzard = main:main",
        ],
    },
)

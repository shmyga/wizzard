from PIL import Image

from wizzard.convert import Converter
from wizzard.convert.preset import ImagePreset
from wizzard.media import MediaType
from wizzard.media.file import MediaFile


class ImageConverter(Converter[ImagePreset]):

    def __init__(self, result_type: MediaType, image_format: str):
        super().__init__(result_type)
        self.image_format = image_format

    def convert(self, *resources: MediaFile, outputfile: MediaFile, preset: ImagePreset = None) -> MediaFile:
        image = Image.open(resources[0].filename)
        quality = preset.quality if (preset is not None and preset.quality) else 100
        image.convert('RGB').save(outputfile.filename, format=self.image_format, quality=quality)
        return outputfile

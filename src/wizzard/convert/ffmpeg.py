import os
from typing import List

from wizzard.convert import Converter
from wizzard.convert.preset import MediaPreset
from wizzard.media import MediaType
from wizzard.media.file import MediaFile
from wizzard.media.meta import MediaMeta, ImageMeta


class VideoFilter:

    def compile(self) -> str:
        raise NotImplementedError

    def __repr__(self):
        return self.compile()


class ScaleFilter(VideoFilter):

    def __init__(self, source, to=(640, 360)):
        self.source_width = source[0]
        self.source_height = source[1]
        self.width = to[0]
        self.height = to[1]
        s = min(float(self.width) / self.source_width, float(self.height) / self.source_height)
        self.scaled_width = int(self.source_width * s)
        self.scaled_height = int(self.source_height * s)

    def compile(self) -> str:
        return "scale=%d:%d,pad=%d:%d:%d:%d:black" % (
            self.scaled_width, self.scaled_height,
            self.width, self.height,
            int((self.width - self.scaled_width) / 2),
            int((self.height - self.scaled_height) / 2),
        )


class FFMpegParams:
    params = (
        ("video_codec", "c:v", None),
        ("video_bitrate", "b:v", None),
        ("video_profile", "profile:v", None),
        ("video_filter", "vf", None),
        ("video_none", "vn", None),

        ("audio_codec", "c:a", None),
        ("audio_bitrate", "b:a", None),
        ("audio_profile", "profile:a", None),
        ("audio_none", "an", None),

        ("channels", "ac", None),
        ("frequency", "ar", None),

        ("preset", "preset", None),
        ("shortest", "shortest", None),
        ("duration", "t", None),
        ("strict", "strict", "experimental"),
        ("loglevel", "v", "fatal"),
    )

    __slots__ = [field for field, _, _ in params]

    def __init__(self, **kwargs):
        for field, _, default in self.params:
            setattr(self, field, kwargs.get(field, default))

    @property
    def __dict__(self):
        return {field: getattr(self, field) for field in self.__slots__}

    def extends(self, **kwargs) -> 'FFMpegParams':
        return FFMpegParams(**{**self.__dict__, **kwargs})

    def compile(self) -> str:
        result = {key: getattr(self, field) for field, key, _ in self.params if getattr(self, field) is not None}
        return " ".join("-%s %s" % (k, v) for k, v in result.items())


class FFMpeg:

    @classmethod
    def execute(cls, inputfiles: List[str], outputfile: str, preset: FFMpegParams) -> bool:
        # add loop for additional inputs
        inputs = (" ".join(
            "%s -i \"%s\"" % ("-loop 1" if index > 0 else "", name) for index, name in enumerate(inputfiles)
        ))
        command = "ffmpeg -y %s %s \"%s\"" % (inputs, preset.compile(), outputfile)
        return os.system(command) == 0


class FFMpegConverter(Converter[MediaPreset]):

    def __init__(self, result_type: MediaType, params: FFMpegParams):
        super().__init__(result_type)
        self.params = params

    def convert(self, *resources: MediaFile, outputfile: MediaFile, preset: MediaPreset = None) -> MediaFile:
        # outputfile = tmpfilename(self.result_type)
        duration = max(resource.meta.length for resource in resources if isinstance(resource.meta, MediaMeta))
        if duration:
            params = self.params.extends(
                duration=duration,
            )
        else:
            params = self.params.extends()
        if preset is not None:
            if preset.channels is not None:
                params.channels = preset.channels
            if preset.bitrate:
                params.audio_bitrate = preset.bitrate
            if preset.frequency:
                params.frequency = preset.frequency
            if preset.codec:
                params.audio_codec = preset.codec
            if preset.duration and duration > preset.duration:
                params.duration = preset.duration
            if preset.size:
                meta = next((resource.meta for resource in resources if isinstance(resource.meta, ImageMeta)), None)
                if meta:
                    params.video_filter = ScaleFilter((meta.width, meta.height), preset.size)
            if not preset.video:
                params.video_none = ""
        if FFMpeg.execute([r.filename for r in resources], outputfile.filename, params):
            return outputfile
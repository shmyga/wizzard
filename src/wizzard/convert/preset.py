

class Preset:
    pass


class MediaPreset(Preset):

    def __init__(self, channels: int = None, bitrate: int = None, frequency: int = None, codec: str = None, duration: int = None, size: tuple = None, video: bool = True):
        self.channels = channels
        self.bitrate = bitrate
        self.frequency = frequency
        self.codec = codec
        self.duration = duration
        self.size = size
        self.video = video


class ImagePreset(Preset):

    def __init__(self, quality: int):
        self.quality = quality

from typing import TypeVar, Generic

from wizzard.convert.preset import Preset
from wizzard.media import MediaType
from wizzard.media.file import MediaFile

P = TypeVar('P', bound=Preset)


class Converter(Generic[P]):

    def __init__(self, result_type: MediaType):
        self.result_type = result_type

    def convert(self, *resources: MediaFile, outputfile: MediaFile, preset: P = None) -> MediaFile:
        raise NotImplementedError

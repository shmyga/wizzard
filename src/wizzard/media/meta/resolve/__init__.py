import os
from typing import TypeVar, Generic

from wizzard.media.meta import Meta

M = TypeVar('M', bound=Meta)


class MetaResolver(Generic[M]):

    def resolve_size(self, filename: str) -> int:
        return os.stat(filename).st_size

    def resolve(self, filename: str) -> M:
        raise NotImplementedError

from PIL import Image

from wizzard.media.meta import ImageMeta
from wizzard.media.meta.resolve import MetaResolver


class ImageResolver(MetaResolver[ImageMeta]):

    def resolve(self, filename: str) -> ImageMeta:
        image = Image.open(filename)
        width, height = image.size
        return ImageMeta(
            self.resolve_size(filename),
            width, height,
        )


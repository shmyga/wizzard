from typing import Type

from mutagen import FileType, File

from wizzard.media.meta import MediaMeta
from wizzard.media.meta.resolve import MetaResolver


class MutagenResolver(MetaResolver[MediaMeta]):

    def __init__(self, *filetypes: Type[FileType]):
        self.filetypes = filetypes

    def resolve(self, filename: str) -> MediaMeta:
        media = File(filename, options=self.filetypes)
        return MediaMeta(
            self.resolve_size(filename),
            media.info.bitrate if media else 0,
            media.info.sample_rate if media else 0,
            media.info.length if media else 0,
        )
from mutagen.aac import AAC
from mutagen.mp3 import MP3
from mutagen.mp4 import MP4
from mutagen.oggflac import OggFLAC
from mutagen.oggspeex import OggSpeex
from mutagen.oggtheora import OggTheora
from mutagen.oggvorbis import OggVorbis

from wizzard.media import MediaType
from wizzard.media.meta import Meta
from wizzard.media.meta.resolve import MetaResolver
from wizzard.media.meta.resolve.image import ImageResolver
from wizzard.media.meta.resolve.mutagen import MutagenResolver


class DefaultMetaResolver(MetaResolver):
    _resolvers = {
        MediaType.MP3: MutagenResolver(MP3),
        MediaType.OGG: MutagenResolver(OggTheora, OggSpeex, OggVorbis, OggFLAC),
        MediaType.AAC: MutagenResolver(AAC),
        MediaType.AVI: MutagenResolver(),
        MediaType.MP4: MutagenResolver(MP4),
        MediaType.PNG: ImageResolver(),
        MediaType.JPG: ImageResolver(),
        MediaType.JPEG: ImageResolver(),
        MediaType.GIF: ImageResolver(),
    }

    def resolve(self, filename: str) -> Meta:
        type_ = MediaType.for_filename(filename)
        return self._resolvers[type_].resolve(filename) if type_ in self._resolvers else None

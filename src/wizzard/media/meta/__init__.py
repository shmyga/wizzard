
class Meta:
    __slots__ = ("size",)

    def __init__(self, size: int):
        self.size = size

    @property
    def __dict__(self):
        return {field: getattr(self, field) for field in self.__slots__}

    def __repr__(self):
        return ", ".join("%s=%s" % (k, v) for k, v in self.__dict__.items())


class MediaMeta(Meta):
    __slots__ = ("size", "bitrate", "sample_rate", "length")

    def __init__(self, size: int, bitrate: int, sample_rate: int, length: float):
        super().__init__(size)
        self.bitrate = bitrate
        self.sample_rate = sample_rate
        self.length = round(length, 2)


class ImageMeta(Meta):
    __slots__ = ("size", "width", "height")

    def __init__(self, size: int, width: int, height: int):
        super().__init__(size)
        self.width = width
        self.height = height

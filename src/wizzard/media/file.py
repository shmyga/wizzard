import hashlib
import os
import pathlib

from wizzard.media import MediaType
from wizzard.media.meta import Meta
from wizzard.media.meta.resolve.default import DefaultMetaResolver


class MediaFile:
    _meta_resolver = DefaultMetaResolver()

    def __init__(self, filename: str):
        self.filename = filename
        self.type = MediaType.for_filename(self.filename)
        self._meta = None

    @property
    def exists(self) -> bool:
        return os.path.exists(self.filename)

    def _resolve_meta(self):
        return self._meta_resolver.resolve(self.filename)

    @property
    def meta(self) -> Meta:
        if self._meta is None:
            self._meta = self._resolve_meta()
        return self._meta

    @property
    def md5(self) -> str:
        hash_md5 = hashlib.md5()
        with open(self.filename, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        return hash_md5.hexdigest()

    @property
    def tmpfile(self) -> "MediaFile":
        *filepath, filename = self.filename.split("/")
        return MediaFile("/".join(filepath + [f"~{filename}"]))

    @property
    def title(self) -> str:
        return self.filename.split("/")[-1]

    def mkdir(self):
        pathlib.Path("/".join(self.filename.split("/")[:-1])).mkdir(parents=True, exist_ok=True)

    def rename(self, filename: str):
        os.rename(self.filename, filename)
        self.filename = filename

    def delete(self):
        if self.exists:
            os.remove(self.filename)

    def __repr__(self):
        return self.filename

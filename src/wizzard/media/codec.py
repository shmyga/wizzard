from enum import Enum


class VideoCodec(Enum):
    libx264 = "libx264"


class AudioCodec(Enum):
    aac = "aac"

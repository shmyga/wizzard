import os
from enum import Enum


class MediaType(Enum):
    MP3 = ".mp3"
    OGG = ".ogg"
    AAC = ".aac"
    WEBM = ".webm"
    AVI = ".avi"
    MP4 = ".mp4"
    PNG = ".png"
    JPG = ".jpg"
    JPEG = ".jpeg"
    GIF = ".gif"
    UNKNOWN = None

    @classmethod
    def _missing_(cls, value):
        return cls.UNKNOWN

    @classmethod
    def for_filename(cls, filename: str) -> "MediaType":
        _, extension = os.path.splitext(filename)
        return MediaType(extension)

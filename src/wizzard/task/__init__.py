from wizzard.convert import Converter
from wizzard.media.file import MediaFile


class Task:

    def __init__(self, inputfile: MediaFile, outputfile: MediaFile):
        self.inputfile = inputfile
        self.outputfile = outputfile

    def run(self, converter: Converter):
        self.outputfile.mkdir()
        self.outputfile.tmpfile.delete()
        result = converter.convert(self.inputfile, outputfile=self.outputfile.tmpfile)
        if result:
            result.rename(self.outputfile.filename)

    def __repr__(self):
        return f"{self.__class__.__name__}(\"{self.inputfile}\" -> \"{self.outputfile}\")"

import os
from typing import Union, Tuple, Iterable

from wizzard.media import MediaType
from wizzard.media.file import MediaFile
from wizzard.task import Task

SourceType = Union[MediaType, Tuple[MediaType]]


class TaskBuilder:

    @classmethod
    def is_sourcetype(cls, filename: str, sourcetype: SourceType) -> bool:
        if isinstance(sourcetype, tuple):
            for item in sourcetype:
                if cls.is_sourcetype(filename, item):
                    return True
        elif isinstance(sourcetype, MediaType):
            return filename.endswith(sourcetype.value)
        return False

    @classmethod
    def build(cls, inputdir: str, outputdir: str, sourcetype: SourceType, resulttype: MediaType) -> Iterable[Task]:
        for dirpath, dirnames, filenames in os.walk(inputdir):
            for filename in filenames:
                if cls.is_sourcetype(filename, sourcetype):
                    inputfile = MediaFile(os.path.join(dirpath, filename))
                    outfilename = ".".join(filename.split(".")[:-1] + [resulttype.value[1:]])
                    outputfile = MediaFile(os.path.join(outputdir, dirpath.replace(inputdir, ""), outfilename))
                    if not outputfile.exists:
                        yield Task(inputfile, outputfile)

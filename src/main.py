import argparse
import tqdm

from wizzard.convert.ffmpeg import FFMpegConverter, FFMpegParams
from wizzard.media import MediaType
from wizzard.task.builder import TaskBuilder


video_bitrate = {
    "640x480": "896k",
    "720x528": "1408k",
    "720x400": "1088k",
}


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("input")
    # parser.add_argument("output")
    args = parser.parse_args()
    print(f"convert \"{args.input}\"")
    converter = FFMpegConverter(MediaType.MP4, FFMpegParams(
        video_codec="libx264",
        video_bitrate="896k",
        video_profile="baseline",
        audio_codec="aac",
        audio_bitrate="192k",
    ))
    tasks = TaskBuilder.build(
        args.input,
        f"{args.input} (converted)",
        MediaType.AVI,
        MediaType.MP4,
    )
    pbar = tqdm.tqdm(list(tasks))
    for task in pbar:
        pbar.set_description(task.inputfile.title)
        task.run(converter)


if __name__ == "__main__":
    main()
